# Generated by Django 3.2.10 on 2022-03-28 09:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0051_userapppermissionresult'),
    ]

    operations = [
        migrations.AddField(
            model_name='permission',
            name='parents',
            field=models.JSONField(blank=True, default=[]),
        ),
        migrations.AddField(
            model_name='permission',
            name='sort_id',
            field=models.IntegerField(default=-1, verbose_name='sort_id'),
        ),
        migrations.AddField(
            model_name='permissiongroup',
            name='parents',
            field=models.JSONField(blank=True, default=[]),
        ),
        migrations.AddField(
            model_name='permissiongroup',
            name='sort_id',
            field=models.IntegerField(default=-1, verbose_name='sort_id'),
        ),
    ]
