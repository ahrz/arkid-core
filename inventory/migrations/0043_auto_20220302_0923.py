# Generated by Django 3.2.10 on 2022-03-02 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0042_auto_20220302_0653'),
    ]

    operations = [
        migrations.AlterField(
            model_name='permission',
            name='action',
            field=models.CharField(blank=True, default='', max_length=256, null=True, verbose_name='动作'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='description',
            field=models.CharField(blank=True, default='', max_length=512, null=True, verbose_name='描述'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='operation_id',
            field=models.CharField(blank=True, default='', max_length=256, null=True, verbose_name='操作id'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='permission_category',
            field=models.CharField(blank=True, default='', max_length=64, null=True, verbose_name='权限类型'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='request_type',
            field=models.CharField(blank=True, default='', max_length=256, null=True, verbose_name='请求方法'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='request_url',
            field=models.CharField(blank=True, default='', max_length=256, null=True, verbose_name='请求地址'),
        ),
    ]
