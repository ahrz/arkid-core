# Generated by Django 3.2.10 on 2022-02-21 08:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0034_alter_permission_is_update'),
    ]

    operations = [
        migrations.RenameField(
            model_name='permissiongroup',
            old_name='permission_parent',
            new_name='parent',
        ),
    ]
